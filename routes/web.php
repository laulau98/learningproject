<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\UserController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'UserController@show');
Route::get('/add/user', 'UserController@addForm');
Route::post('/add/user', 'UserController@add');
Route::get('/edit/user/{id}', 'UserController@editForm');
Route::post('/edit/user/{id}', 'UserController@edit');
Route::delete('/delete/user/{id}', 'UserController@delete');

