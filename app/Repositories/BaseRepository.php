<?php


namespace App\Repositories;


class BaseRepository
{
    /**
     * The Model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Destroy a model.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

    /**
     * Get Model by id.
     *
     * @param  int  $id
     * @return \App\Models\Model
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }


    public function getByIdUserId($id, $user_id)
    {
        return $this->model
            ->firstWhere(["id" => $id, "user_id" => $user_id]);
    }

    public function getByIdUserIdActive($id, $user_id)
    {
        return $this->model
            ->firstWhere(['is_actif' => 1, "id" => $id, "user_id" => $user_id]);
    }

    /**
     * @return array
     */
    public function getFillable()
    {
        return $this->model->getFillable();
    }

    public function create($fields)
    {
        $model = new $this->model;
        foreach ($fields as $key => $field) {
            $model->$key = $field;
        }
        $model->save();
        return $model;
    }

    public function update($model, $fields)
    {
        foreach ($fields as $key => $field) {
            $model->$key = $field;
        }
        $model->save();
        return $model;
    }

    public function disable($model)
    {
        $model->is_actif = 0;
        $model->save();
        return $model;
    }

    public function delete($model)
    {
        return $model->delete();
    }
}