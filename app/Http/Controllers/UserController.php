<?php


namespace App\Http\Controllers;


use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

/**
 * @OA\Get(
 * path="/users",
 * summary="display all users",
 * description="the list of users",
 * operationId="usersList",
 * tags={"users"},
 * @OA\Response(
 *    response=200,
 *    description="An Array of users",
 *    @OA\JsonContent(
 *       type="object",
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *   )
 * ),
 * @OA\Get(
 * path="/add/user",
 * summary="Create user form",
 * description="Route to access the user creation form",
 * operationId="formcreation",
 * tags={"add user"},
 * @OA\RequestBody (
 *  required = true,
 *  description = "Ajout des informations d'un nouvel utilisateur", *
 *  @OA\JsonContent (
 *     required = {"name", "email"},
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *  ),
 * @OA\Response(
 *    response=200,
 *    description="acces to the form",
 *   )
 * ),
 * @OA\Post(
 * path="/add/user",
 * summary="store user method",
 * description="save new user in database",
 * operationId="saveUser",
 * tags={"add user BDD"},
 * @OA\RequestBody (
 *  required = true,
 *  description = "Enregistrer les informations du nouvel utilisateurs",
 *  @OA\JsonContent (
 *     required = {"name", "email"},
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *  ),
 * @OA\Response(
 *    response=201,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *   )
 * ),
 *@OA\Get(
 * path="/edit/user/{id}",
 * summary="edit user form",
 * description="Route to access the user edit form",
 * operationId="formedit",
 * tags={"edit user"},
 * @OA\RequestBody (
 *  required = true,
 *  description = "Edition des informations d'un utilisateur",
 *  @OA\JsonContent (
 *     required = {"id"},
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *  ),
 * @OA\Response(
 *    response=200,
 *    description="acces to the form",
 *   )
 * ),
 * @OA\Post(
 * path="/edit/user/{id}",
 * summary="update user method",
 * description="update user in database",
 * operationId="updateUser",
 * tags={"update user BDD"},
 * @OA\RequestBody (
 *  required = true,
 *  description = "mettre à jour les informations d'un utilisateurs",
 *  @OA\JsonContent (
 *     required = {"id"},
 *       @OA\Property(property="id", type="int"),
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *  ),
 * @OA\Response(
 *    response=201,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="id", type="int"),
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *   )
 * ),
 * @OA\Delete(
 * path="/delete/user/{id}",
 * summary="delete user method",
 * description="delete user in database",
 * operationId="deleteUser",
 * tags={"delete user BDD"},
 * @OA\RequestBody (
 *  required = true,
 *  description = "supprimer les informations d'un utilisateurs",
 *  @OA\JsonContent (
 *     required = {"id"},
 *       @OA\Property(property="id", type="int"),
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *  ),
 * @OA\Response(
 *    response=202,
 *    description="success",
 *    @OA\JsonContent(
 *       @OA\Property(property="id", type="int"),
 *       @OA\Property(property="name", type="string", format="text"),
 *       @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
 *    ),
 *   )
 * ),
 *
 */

class UserController extends Controller
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function show()
    {
        $users = $this->userRepository->all();
        $data = [] ;
        $data['users'] = $users;
        return view('index', $data);
    }

    public function addForm() {
        return view('form');
    }

    public function add(Request $request) {
        $request->validate([
            'user-name' => 'required',
            'user-email' => 'required',
        ]);
        /*if ($validator->fails()){
            $request->session()->flash('message_error', 'Sauvegarde impossible.');
            return redirect( $request->url() )
                ->withInput()
                ->withErrors($validator);
        }*/
        $fields = new User();
        $fields->name = $request->input('user-name');
        $fields->email = $request->input('user-email');
        $fields->save();

        return redirect( url('/users') );
    }

    public function editForm($id) {
        $user = $this->userRepository->getById($id);
        return view('form', $user);
    }

    public function edit($id, Request $request) {
        $user = $this->userRepository->getById($id);
        $user->name = $request->input('user-name');
        $user->email = $request->input('user-email');
        $user->save();

        return redirect( url('/users') );
    }

    public function delete($id) {
        $this->userRepository->destroy($id);
        return redirect(url('/users') );
    }
}